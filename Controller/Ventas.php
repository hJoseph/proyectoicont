<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuario = $_GET['usuario'];
$contrasena = $_GET['password'];

$con = new conexion();
$userRegister = $con->getUser($usuario, $contrasena);

foreach ($userRegister as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $foto = $user['foto'];
}


if ($tipo == 'ADMINISTRADOR') {

    $tipo = $tipo;
    $showStatus = $con->getStatus();
    foreach ($showStatus as $status) {
        $estado = $status['estado'];
    }

    $menuMain = $con->getMenu();
    $_SESSION['id_usuario'] = $id_usuario;
    $_SESSION['nombres'] = $nombres;

    $showAllProduct = $con->getProduct();

    $showPreVenta = $con->getPreVenta();

    $totalPreVenta = $con->getTotalPreVenta();

    require('../Views/VentasPos.php');

}


?>