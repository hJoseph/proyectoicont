<section class="panel">

    <header class="panel-heading" align="center">
        PRODUCTOS SOLICITADOS
    </header>
    <table class="table table-striped">
        <thead>
        <tr>
            <td width="20">Imagen</td>
            <td>Producto</td>
            <td>Cant.</td>
            <td>Precio</td>
            <td>Total</td>
            <td>Tipo</td>
            <td>Opcion</td>
        </tr>
        <?php
        foreach ($showPreVenta

        as $preventa) {
        ?>
        <tr>
            <td><img src="<?PHP echo url();
                echo '/Views/';
                echo $preventa['imagen'] ?>" width="60" height="60"></td>
            <td><b><?php echo $preventa['producto']; ?></b></td>
            <td><?php echo $preventa['cantidad']; ?></td>
            <td><?php echo $preventa['precio']; ?></td>
            <td><?php echo $preventa['totalPrecio']; ?></td>
            <td><?php echo $preventa['tipo']; ?></td>
            <td>
                <?PHP echo "<a style=\"cursor:pointer;\"  class='btn btn-success'   
                               onclick=\"pedirDatosCompra('" . $preventa['idproducto'] . "')\">
                               <i class='icon_plus_alt2'></i></a>"; ?>
                               <a onclick="deleteDatoP('<?PHP echo $preventa['idpreventa']; ?>')" data-name="Mouse"
                               class="btn btn-danger" data-toggle="modal"><i class="icon_close_alt2"></i>
                             </a>
            </td>
         <?php  }  ?>
        </tr>

        <tr>
            <td colspan="3"></td>
            <td> Total :</td>
            <td>
                <b>
                    <h2>
                        <?php foreach ($totalPreVenta as $totalVenta) {
                            echo $totalVenta['total'];
                        } ?>
                    </h2>
                </b>
            </td>
        </tr>

        <tr>
            <td colspan="4" align="center">
                <a data-toggle="modal" class="btn btn-primary enabled" href="facturarN.php" data-target="#myModal">
                    <strong> ACEPTAR</strong></a>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        </div><!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </td>
            <td colspan="3" align="rigth">
                <a  class="btn btn-danger enabled" href="facturarN.php" >
                    <strong> CANCELAR</strong></a>
            </td>
        </tr>

        </thead>
        <div id="formulario" style="display:none;">
        </div>
    </table>
    </div>
    <!-- /.table-responsive -->
    </div>
    <!--main content end-->
</section>