﻿/**
 * Copyright (C) 2005-2015 TeamViewer GmbH. All rights reserved.
 *
 * This is unpublished proprietary source code of TeamViewer
 * GmbH. The contents of this file may not be disclosed to third
 * parties, copied or duplicated in any form, in whole or in part,
 * without the prior written permission of TeamViewer GmbH.
 * For further regulations see the file "license.txt".
 */


(function () {

	window.wsp = window.wsp || {
		tracks: []
	};

	if (!window.$) {
		throw "wsp needs jquery";
	}

	if (!window.wsp.config || !window.wsp.config.project) {
		throw "please use window.wsp.config.project to configure the project name";
	}

	var url = window.wsp.config.url || "//wsp.teamviewer.com/t/";

	var trackTypes = window.wsp.trackTypes = {
		Flume: 1,
		GAPageView: 2,
		GAEvent: 3
	};

	var toJSON = $.toJSON || JSON.stringify;

	var getLoadingTime = function () {
		var deferred = $.Deferred();

		var tryGetLoadingTime = function () {
			if (typeof window.performance !== "undefined") {
				var loadingTime = window.performance.timing.loadEventEnd - window.performance.timing.navigationStart;
				if (loadingTime > 0) {
					deferred.resolve(loadingTime);
					clearInterval(loadingTimeinterval);
				}
			} else {
				clearInterval(loadingTimeinterval);
			}
		};

		var loadingTimeinterval = setInterval(tryGetLoadingTime, 500);

		return deferred;
	};

	var PARTITION_SIZE = 5;

	function partitionArray(arr, partitionSize) {
		return arr.reduce(function (res, curr, i) {
			if (i % partitionSize === 0) {
				res.push([]);
			}

			res[res.length - 1].push(curr);
			return res;
		}, []);
	}

	function send(events) {
		var data = events.map(function (event) {
			var type = event.type;

			// If no type is present we assume this is a flume event.
			// GA events will have a type explicitly set.
			if (!type) {
				type = trackTypes.Flume;
			} else {
				// Delete the type so it doesn't get serialized with the event
				// (not needed because the outer event wrapper will contain the type)
				delete event.type;
			}

			switch (type) {
				case trackTypes.Flume:
					return flumeEvent(event);

				case trackTypes.GAPageView:
				case trackTypes.GAEvent:
					return ga(type, event);
			}
		}).filter(Boolean);

		if (data.length === 0) {
			return;
		}

		var partitionedData = partitionArray(data, PARTITION_SIZE);

		for (var i = 0; i < partitionedData.length; i++) {
			(function (i) {
				var dataToSend = partitionedData[i];
				var $img = $('<img style="position: fixed;">');

				$img.attr("src", url + window.wsp.config.project + "?" + $.param({
					evs: toJSON(dataToSend),
					t: Date.now()
				}));

				$img.on("load", function () {
					$img.remove();
				});

				$img.on("error", function () {
					$img.remove();
				});

				$("body").append($img);
			})(i);
		}
	};

	function flumeEvent(data) {
		if (data.eventName && data.eventName.match(/pageload/i) && !data.value && typeof window.performance !== "undefined") {
			getLoadingTime().done(function (time) {
				data.value = time;
				window.wsp.tracks.push(data);
			});
			return;
		}

		var defaultData = {
			Height: screen.height,
			Width: screen.width,
			PixelRatio: window.devicePixelRatio || 1
		};

		if (typeof data.payload === "object") {
			data.payload = toJSON(data.payload);
		}

		var finalData = {
			t: trackTypes.Flume,
			e: toJSON($.extend(defaultData, wsp.config.defaultData, data))
		};

		return finalData;
	}

	function ga(type, data) {
		if (!window.wsp.config.ga) {
			return;
		}

		data = $.extend(data, {
			tid: window.wsp.config.ga.tid,
			ds: window.wsp.config.ga.source,
			dh: window.location.hostname
		});

		var finalData = {
			t: type,
			e: toJSON(data)
		};

		return finalData;
	}

	window.wsp.track = function (data) {
		window.wsp.tracks.push(data);
	};

	$(function () {
		var flush = function () {
			if (window.wsp.tracks.length === 0) {
				return;
			}

			var tracks = window.wsp.tracks;
			window.wsp.tracks = [];

			send(tracks);
		};

		setInterval(flush, 250);
		$(window).bind("beforeunload", flush);
	});
})();
