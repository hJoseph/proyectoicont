<?php

class conexion
{

    private $user;
    private $password;
    private $server;
    private $database;
    private $con;

    public function __construct()
    {
        $user = 'root';
        $server = "localhost";
        $database = "icontcarlitos";
        $password = '';
        $this->con = new mysqli($server, $user, $password, $database);
    }


    public function getAllUsers()
    {
        $query = $this->con->query('SELECT nombre, password,tipo FROM usuarios');

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }

        return $retorno;
    }


    public function getUser($usuario, $contrasena)
    {
        $query = $this->con->query("SELECT * FROM usuarios WHERE login='" . $usuario . "' AND password='" . $contrasena . "'");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getStatus()
    {
        $query = $this->con->query("SELECT * FROM `menu` where idmenu='1'");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getMenu()
    {
        $query = $this->con->query("SELECT * FROM `menu` ");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getProduct()
    {
        $query = $this->con->query("SELECT * FROM producto");
        return $query;
    }

    public function getPreVenta()
    {
        $query = $this->con->query("SELECT idpreventa,imagen,producto, count(producto) AS cantidad, sum(precio) as totalPrecio, idproducto, pventa, idUser, precio ,tipo
                                           FROM `preventa` GROUP BY producto , idproducto ,tipo ORDER BY idpreventa ASC");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getTotalPreVenta()
    {
        $query = $this->con->query("SELECT SUM( precio ) as total  FROM `preventa`");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }


    public function deleteUnaPreVenta($idproducto)
    {
        $query = $this->con->query("DELETE FROM `preventa` WHERE `preventa`.`idPreventa` = '$idproducto");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }


}

?>